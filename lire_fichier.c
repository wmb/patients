#include "general_funcs.h"
#include "lire_fichier.h"
#include "struct.h"
#include "liberer.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* Le prof nous a dit qu'on peut supposer qu'une ligne est < 300
 * (+1 pour le '\n', et un autre +1 pour le '\0' */
#define MAX_LINE_LENGTH (300 + 2)

/**
 * Cette fonction lis un mot (regex /[^[:space:]]+/) de buf,
 * buf        ::= [whitespace] mot [whitespace]
 * mot        ::= caractère [mot]
 * caractère  ::= n'import quel caractère affichable (sauf les blancs)
 * whitespace ::= '\n' | ' ' | '\t' | '\f'
 * alloue de l'espace pour ce mot, et le copie dans cet espace,
 * puis elle pointe *mot vers ce mot.
 * Elle retourne la position de buf après la lecture du mot.
 */
size_t lire_mot(const char *buf, char **mot)
{
	size_t compteur_longueur_mot;
	size_t indice_buf;
	size_t mot_start;
	size_t i;

	/* Sanity check */
	if (!buf && !mot)
		return 0;

	/* Skip whitespace */
	for (mot_start = 0; isspace(buf[mot_start]); ++mot_start)
		continue;

	for (indice_buf = mot_start, compteur_longueur_mot = 0;
		buf[indice_buf] != '\0' && !isspace(buf[indice_buf]);
		++indice_buf) {
		++compteur_longueur_mot;
	}

	if (compteur_longueur_mot == 0)
		return 0;

	*mot = malloc(compteur_longueur_mot + 1);

	if (!*mot)
		return 0;

	for (i = 0; i < compteur_longueur_mot; ++i)
		*(*mot + i) = buf[mot_start + i];
	*(*mot + compteur_longueur_mot) = '\0';

	return indice_buf;
}

/**
 * Cette fonction remplis pa en utilisant les entrées sur le fichier f.
 */
int lire_fichier_v2(FILE *f, liste_d *pa[])
{
	char ligne[MAX_LINE_LENGTH];
	unsigned char erreur = 0;

	if (!(f && pa))
		return 1;

	while (fgets(ligne, sizeof(ligne) / sizeof(ligne[0]), f)) {
		patient_s *p;
		unsigned char lettre;
		if (strchr(ligne, '\n') == NULL) {
			/* La ligne n'est pas complète car elle est trop
			** longue. */
			fputs("lire_fichier_v2: Erreur: Ligne trop longue.\n",
				stderr);
			/* Indiquer à la prochaine itération que la ligne
			** n'est pas complète. */
			erreur = TRUE;
			continue;
		} else if (erreur) {
			/* Ne pas traîter une ligne non complète */
			erreur = FALSE;
			continue;
		}

		p = lire_ligne(ligne);

		if (!p)
			continue;
		lettre = trouver_lettre((*(p->nomprenom)));
		pa[lettre] = ajouter_patient_trie(pa[lettre], p);
	}

	return 0;
}

/**
 * Cette fonction prend une ligne du fichier f, et crée un patient, et le
 * retourne
 */
patient_s *lire_ligne(const char *ligne)
{
	/* TODO: This function has become too bloated. Split it up. */
	char *nom = NULL;
	char *prenom = NULL;
	char *nomprenom = NULL;
	char *med_nom = NULL;
	char *med_quant = NULL;
	unsigned short quant = 0;
	patient_s *patient = NULL;
	sliste *medlist = NULL;
	size_t nomprenom_longueur = 2;
	size_t pos_ligne = 0;
	size_t b = 0;

	if (!ligne)
		goto cleanup;

	/* Lecture du nom et prénom */
	pos_ligne += lire_mot(ligne + pos_ligne, &nom);
	if (!nom) {
		fputs("lire_ligne: Erreur de lecture du nom.\n", stderr);
		goto cleanup;
	}
	pos_ligne += lire_mot(ligne + pos_ligne, &prenom);
	if (!prenom) {
		fprintf(stderr, "lire_ligne: Erreur de lecture du pr" E_ACUTE
			"nom de %s.\n", nom);
		goto cleanup;
	}

	nomprenom_longueur += strlen(nom);
	nomprenom_longueur += strlen(prenom);
	nomprenom = malloc(nomprenom_longueur);
	if (!nomprenom) {
		fputs("lire_ligne: Erreur d'allocation de nomprenom.\n",
			stderr);
		goto cleanup;
	}
	nomprenom = strcpy(nomprenom, nom);
	free(nom);
	nomprenom = strcat(nomprenom, " ");
	nomprenom = strcat(nomprenom, prenom);
	free(prenom);

	while (pos_ligne < MAX_LINE_LENGTH) {
		medicaments_s *m;
		pos_ligne += (b = lire_mot(ligne + pos_ligne, &med_nom));
		if (!b)
			break;
		pos_ligne += (b = lire_mot(ligne + pos_ligne, &med_quant));
		if (!b) {
			fprintf(stderr, "lire_ligne: Erreur: M" E_ACUTE
				"edicament %s sans quantit" E_ACUTE ".\n",
				med_nom);
			free(med_nom);
			break;
		}
		quant = (unsigned short) strtoul(med_quant, NULL, 10);
		free(med_quant);
		if (quant == 0) {
			fprintf(stderr, "lire_ligne: Erreur: M" E_ACUTE
				"edicament %s a la quantit" E_ACUTE " = 0.\n",
				med_nom);
			free(med_nom);
			continue;
		}
		m = meds_creer_element(med_nom, quant);
		if (m)
			medlist = sliste_ajouter(medlist, m);
	}

	patient = patient_creer_element(nomprenom, medlist);

	return patient;

cleanup:
	if (nom)
		free(nom);
	if (prenom)
		free(prenom);
	if (nomprenom)
		free(nomprenom);
	if (med_nom)
		free(med_nom);
	if (med_quant)
		free(med_nom);
	if (patient)
		free(patient);

	return NULL;
}

/**
 * Cette fonction ajoute un patient à une liste trié, sans changer l'ordre du
 * tri.
 */
liste_d *ajouter_patient_trie(liste_d *liste, patient_s *p)
{
	const char *nom1;
	unsigned char trouve = FALSE;
	unsigned char duplicate = FALSE;
	dliste *l;

	if (liste)
		l = liste->tete;
	else
		return dliste_creer_liste(p);

	if (!p)
		return liste;

	nom1 = p->nomprenom;
	while (l && !trouve && !duplicate) {
		int tmp;
		const char *nom2 = ((patient_s *) l->data)->nomprenom;
		tmp = strcmp(nom1, nom2);
		if (tmp > 0)
			l = l->next;
		else if (tmp < 0)
			trouve = TRUE;
		else
			/* Ne pas ajouter des elements en double. */
			duplicate = TRUE;
	}

	if (duplicate) {
		fprintf(stderr, "ajouter_patient_trie: Patient en double : %s"
			"\n", nom1);
		liberer_patient(p);
		return liste;
	}

	if (trouve)
		liste = dliste_ajouter_avant(liste, l, p);
	else
		liste = dliste_ajouter(liste, p);

	return liste;
}
