gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o consume.o consume.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o dliste.o dliste.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o general_funcs.o general_funcs.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o liberer.o liberer.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o lire_fichier.o lire_fichier.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o main.o main.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o print_v3.o print_v3.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o rechercher.o rechercher.c
gcc -Wall -Wextra -Werror -pedantic-errors -Ofast -std=c99   -c -o sliste.o sliste.c
gcc -o "prj.exe" consume.o dliste.o general_funcs.o liberer.o lire_fichier.o main.o print_v3.o rechercher.o sliste.o   
