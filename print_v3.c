#include "print_v3.h"
#include "general_funcs.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>


/**
 * Affiche la liste des patients, n caractères à la fois (4 par défaut).
 */
void afficher_patients(const liste_d *liste_patients[], short n)
{
	unsigned compteur_patients = 0;

	/* Lettre (indice de liste_patients[] */
	unsigned char l;

	const char *message = "Continuer " A_GRAVE " afficher ?> ";

	/* Si n ≥ 0, alors arrêter pour demander si l'utilisateur veut
	** continuer l'affichage, chaque n patients */
	unsigned char arreter = (n >= 0);

	/* Reponse de l'utilisateur:
	** 0: Arrêter l'affichage et retourner à la fonction appelante,
	** 1: Continuer a afficher, n patients a la fois,
	** 2: Continuer a afficher jusqu'à la fin des patients.
	**/
	unsigned char reponse = 1;

	/* Si n = 0, alors utiliser la valeur par défaut */
	if (n == 0)
		n = 4;

	/* Boucle 0: boucle principal, sur les lettres */
	for (l = 0; l < PATIENT_ARRAY_ENTRIES; ++l) {
		/* Liste des patients commençant par la lettre (l + 'A') */
		const liste_d *lettre = liste_patients[l];

		if (arreter && l && l % n == 0) {
			reponse = lire_reponse(message);
		/* Traîter la réponse de l'utilisateur */
			switch (reponse) {
			case 0:
				/* Arrêter l'affichage
				** et retourner à la fonction appelante. */
				goto sortir;
				break;
			case 1:
				/* Continuer a afficher,
				** n patients a la fois,
				** comme si rien ne s'est passé. */
				break;
			case 2:
				/* Continuer a afficher
				** jusqu'à la fin des patients. */
				arreter = FALSE;
				break;
			default:
				/* Normalement c'est une réponse invalide,
				** alors il n'y a rien a faire */
				break;
			}
		}

		printf("%hu. Lettre: %c\n", (unsigned short) (l + 1),
				trouver_lettre_affichage(l));

		if (!lettre || lettre->nbr_elements == 0) {
			/* Cette lettre n'a pas de patients */
			puts("Pas de patients dans cette lettre.\n");
			continue;
		}

		/* Incrementer le compteur des patients affichés, et afficher
		** les patients de cette lettre, sans leurs médicaments. */
		afficher_liste_patients(lettre, compteur_patients,
			FALSE, TRUE);

		compteur_patients +=  lettre->nbr_elements;

		fputc('\n', stdout);
	} /* Fin de boucle 0 */

sortir:
	return;
}

/**
 * Affiche une dliste des patients, sans afficher leurs médicaments.
 */
void afficher_liste_patients(const liste_d *liste_patients, unsigned n,
	unsigned char afficher_meds, unsigned char afficher_numeros)
{
	unsigned compteur_patients = n;

	if (!liste_patients || liste_patients->nbr_elements == 0)
		return;

	const dliste *l = liste_patients->tete;

	/* Boucle 0: boucle principal, sur les patients */
	while (l) {
		const patient_s *const p = l->data;

		/* Afficher le patient p, sans ses médicaments */
		++compteur_patients;
		if (afficher_numeros)
			afficher_un_patient(p, compteur_patients, afficher_meds);
		else
			afficher_un_patient(p, -1, afficher_meds);

		/* Avancer */
		l = l->next;
	} /* Fin de boucle 1 */

	return;
}

/**
 * Afiche une seule entrée dans la liste des patients (un patient).
 */
void afficher_un_patient(const patient_s *patient, int n,
	unsigned char afficher_meds)
{
	/* Verifier qu'il n y'a pas d'erreurs */
	if (!patient) {
		fputs("afficher_un_patient: Patient est NULL!\n", stderr);
		return;
	}
	if (!(patient->nomprenom)) {
		fputs("afficher_un_patient: Nom du patient est vide!\n",
			stderr);
		return;
	}

	/* Si n > 0 alors afficher le numéro du patient */
	if (n > 0)
		printf("%d. ", n);

	/* Si afficher_meds == vrai, ne pas affchier "nom/prenom:" */
	if (afficher_meds)
		fputs("Nom/pr" E_ACUTE "nom : ", stdout);

	printf("%s", patient->nomprenom);

	if (afficher_meds) {
		if (!(patient->medicaments)) {
			puts(",\n\t(Pas de m" E_ACUTE "dicaments).");
			return;
		}

		puts(",\n\tM" E_ACUTE "dicaments :\n\t\tQte Nom");
		afficher_medicaments(patient->medicaments);
	}
	putchar('\n');

	return;
}

/**
 * Affiche la liste des medicaments pour un patient.
 */
void afficher_medicaments(const sliste *meds)
{
	/* On sait que meds != NULL */
	while (meds) {
		const medicaments_s *const m = (medicaments_s *) meds->data;
		if (m) {
			afficher_un_medicament(m);
		}
		meds = meds->next;
	}

	return;
}

/**
 * Affiche un seul medicament.
 */
void afficher_un_medicament(const medicaments_s *med)
{
	/* On sait que med != NULL */

	if (med->quant == 0)
		return;	/* Ne pas afficher si la quantité est zéro */
	if (med->medicam == NULL)
		return;	/* Erreur: Le nom du médicament
			** ne peut pas être NULL */

	printf(
		"\t\t"	/* Indentation */
		"%3hu "	/* Quantite du médicament */
		"%s" 	/* Le nom du médicament */
		"\n",	/* Retour à la ligne */
		med->quant,
		med->medicam
	);

	return;
}

/**
 * Lis la réponse de l'utilisateur (continuer a afficher ou pas).
 */
unsigned char lire_reponse(const char *question)
{
	int r = -1;
	char buffer[0x10];
	char *ptr;

	fputs(question, stdout);

	if (fgets(buffer, sizeof buffer, stdin) == NULL) {
		/* Erreur de lecture. Quitter. */
		fputs("lire_reponse: Erreur de lecture.\n", stderr);
		return 0;
	}
	ptr = buffer;

	if (*ptr == '\n')
		return 1;

	/* Skip whitespace */
	while (*ptr && isspace(*ptr))
		++ptr;

	while (*ptr) {
		if (isdigit(*ptr)) {
			r = *ptr - '0';
			break;
		} else if (*ptr == '\n') {
			r = -1;
			break;
		} else if (*ptr == 'q') {
			r = 0;
			break;
		}
		++ptr;
	}

	if (!(0 <= r && r < 3)) {
		const char *aide = "Veuiller entrer l'un de ces commandes:\n"
			"\t0 ou q : Arr" E_CIRC "ter l'affichage et retourner"
			" " A_GRAVE " la fonction appelante.\n"
			"\t1 ou Entr" E_ACUTE "e (r" E_ACUTE "ponse vide) : "
				"Continuer a afficher, n patients a la fois."
				"\n"
			"\t2 : Continuer a afficher jusqu'" A_GRAVE " la fin "
				"des patients.\n";
		fputs(aide, stderr);
		/* Appeler lire_reponse recursivement pour avoir une réponse
		** valide */
		r = lire_reponse(question);
	}

	return r;
}

