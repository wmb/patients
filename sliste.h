#ifndef PATIENTS_SLISTE_H
# define PATIENTS_SLISTE_H

/* */
/* Liste chainée simple */
struct sliste {
	void *data;
	struct sliste *next;
};
typedef struct sliste sliste;
/* */


/* Ajouter un element à la fin de la liste */
sliste *sliste_ajouter(sliste *list, void *data);

/* Supprimmer l'element elem de la liste list */
sliste *sliste_supprimer(sliste *list, sliste *elem);


#endif /* PATIENTS_SLISTE_H */
