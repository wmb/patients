#include "sliste.h"
#include <stdio.h>

void print_list(sliste *l);

int main(void)
{
	const int n = 10;
	int i;
	sliste *l = NULL;
	sliste *fifth;

	for (i = 1; i <= n; ++i) {
		l = sliste_ajouter(l, (void *) (long) i);
	}
	
	puts("Printing the list:");
	print_list(l);

	puts("Removing first element:");
	l = sliste_supprimer(l, l);
	print_list(l);

	print_list(l);
	puts("Removing the 5th element:");

	fifth = l;
	for (int i = 0; i < 5 - 1 && fifth; ++i) {
		fifth = fifth->next;
	}
	l = sliste_supprimer(l, fifth);

	print_list(l);

	sliste *one_element_list = sliste_ajouter(NULL, (void *) (long) 1337);
	puts("One-element list:");
	print_list(one_element_list);
	puts("Removing that one element:");
	one_element_list = sliste_supprimer(one_element_list, one_element_list);
	print_list(one_element_list);

	return 0;
}

void print_list(sliste *l)
{
	if (!l) {
		puts("Empty list");
		return;
	}
	while (l) {
		printf("%4d ", (int) (long) l->data);
		l = l->next;
	}
	putchar('\n');

	return;
}
