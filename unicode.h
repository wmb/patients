#ifndef PROJET_PATIENTS_UNICODE_H
# define PROJET_PATIENTS_UNICODE_H

#if defined(__WIN32)

# define A_GRAVE	"\xe0"
# define E_GRAVE	"\xe8"
# define E_ACUTE	"\xe9"
# define E_CIRC		"\xea"
# define I_CIRC		"\xee"

# define WINDOWS_FIX_CHARSET() system("CHCP 1252")

#else

# define A_GRAVE	"\u00e0"
# define E_GRAVE	"\u00e8"
# define E_ACUTE	"\u00e9"
# define E_CIRC		"\u00ea"
# define I_CIRC		"\u00ee"

# define WINDOWS_FIX_CHARSET()

#endif

#endif /* PROJET_PATIENTS_UNICODE_H */
