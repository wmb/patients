#ifndef PATIENTS_DLISTE_H
# define PATIENTS_DLISTE_H

/* */
/* Liste chainée doublement-chainée (bidirectionnelle) */
struct dliste {
	void *data;
	struct dliste *next;
	struct dliste *prev;
};
struct liste_d {
	struct dliste *tete;
	struct dliste *queue;
	unsigned short nbr_elements;
};

typedef struct dliste dliste;
typedef struct liste_d liste_d;
/* */


/* Créer une nouvelle liste avec un seul element contenant data */
liste_d *dliste_creer_liste(void *data);

/* Ajouter un element à la fin de la liste */
liste_d *dliste_ajouter(liste_d *list, void *data);

/* Ajouter un element x après elem dans la liste list */
liste_d *dliste_ajouter_apres(liste_d *list, dliste *elem, void *x);

/* Ajouter un element x avant elem dans la liste list */
liste_d *dliste_ajouter_avant(liste_d *list, dliste *elem, void *x);

/* Ajouter un element x à la tête de la liste liste */
liste_d *dliste_ajouter_tete(liste_d *list, void *x);

/* Supprimmer l'element x de la liste list, et retourner un pointeur vers ce
 * qu'il avait dans le champ data. */
void *dliste_supprimer(liste_d *list, dliste *x);

/* Liberer la liste seulement (sans toucher les data) */
liste_d *dliste_liberer_liste(liste_d *liste);


#endif /* PATIENTS_DLISTE_H */
