#include <stdlib.h>
#include "liberer.h"

/**
 * Libère toutes les entrées
 */
void liberer_tout(liste_d *liste_patients[])
{
	/* Lettre (indice pour liste_patients[]) */
	unsigned char l;
	for (l = 0; l < PATIENT_ARRAY_ENTRIES; ++l) {
		liberer_lettre(liste_patients[l]);
	}
	return;
}

/**
 * Libère tous les patients d'une certaine lettre
 */
void liberer_lettre(liste_d *lettre)
{
	if (!lettre)
		return;

	while (lettre->nbr_elements > 0)
		liberer_patient(dliste_supprimer(lettre, lettre->tete));

	free(lettre);

	return;
}

/**
 * Libère un seul patient, et tous ces médicaments
 */
void liberer_patient(patient_s *patient)
{
	if (!patient)
		return;
	if (patient->nomprenom)
		free(patient->nomprenom);
	if (patient->medicaments)
		liberer_liste_meds(patient->medicaments);
	free(patient);
	return;
}

/**
 * Libère la liste des medicaments d'un patient
 */
void liberer_liste_meds(sliste *meds)
{
	if (meds) {
		sliste *tmp = meds->next;
		liberer_medicament(meds->data);
		free(meds);
		liberer_liste_meds(tmp);
	}
	return;
}

void liberer_medicament(medicaments_s *med)
{
	if (!med)
		return;
	if (med->medicam)
		free(med->medicam);
	free(med);
	return;
}
