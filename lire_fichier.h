#ifndef PROJET_PATIENTS_LIRE_FICHIER_H
# define PROJET_PATIENTS_LIRE_FICHIER_H

# include "struct.h"
# include <stdlib.h>

/* Fonctions */

/**
 * Cette fonction lis un mot (regex /[^[:space:]]+/) de buf,
 * alloue de l'espace pour ce mot, et le copie dans cet espace,
 * puis elle pointe *mot vers ce mot.
 * Elle retourne la position de buf après la lecture du mot.
 */
size_t lire_mot(const char *buf, char **mot);

/**
 * Cette fonction remplis pa en utilisant les entrées sur le fichier f
 */
int lire_fichier_v2(FILE *f, liste_d *pa[]);

/**
 * Cette fonction prend une ligne du fichier f, et crée un patient, et le
 * retourne
 */
patient_s *lire_ligne(const char *ligne);

/**
 * Cette fonction ajoute un patient à une liste trié, sans changer l'ordre du
 * tri.
 */
liste_d *ajouter_patient_trie(liste_d *liste, patient_s *p);

#endif /* PROJET_PATIENTS_LIRE_FICHIER_H */
