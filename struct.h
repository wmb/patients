#ifndef MY_STRUCT_H
# define MY_STRUCT_H

/* Pour afficher des caractère accentués comme é, à, æ, etc. */
#include "unicode.h"


/* 27 elements pour les 26 lettres de l'alphabets,
 * plus un elements pour tous les autres caractères */
# define PATIENT_ARRAY_ENTRIES 27

/* Definir TRUE est FALSE */
# ifndef TRUE
#  define TRUE 1
# endif /* TRUE */
# ifndef FALSE
#  define FALSE 0
# endif /* FALSE */


/* Definition des structures de donneés */

/* Liste chainée simple */
# include "sliste.h"

/* Liste chainée bidirectionnelle (doublement-chainée) */
# include "dliste.h"

/* Structure pour les medicaments */
struct medicaments_s {
	unsigned short quant;
	char *medicam;
};

/* Structure pour les patients */
struct patient_s {
	sliste *medicaments;
	char *nomprenom;
};


/* Declaration des types
 * (pour ne pas avoir à écrire "struct" à chaque fois) */

typedef struct medicaments_s medicaments_s;
typedef struct patient_s patient_s;
typedef struct liste_patients liste_patients;


#endif /* MY_STRUCT_H */
