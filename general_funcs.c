#include "struct.h"
#include "general_funcs.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static char *program_name = NULL;

void set_program_name(char *pname)
{
	if (!program_name)
		program_name = pname;
	return;
}

char *get_program_name(void)
{
	return program_name;
}


/* Trouver la quel element de pa va avec la lettre c */
unsigned char trouver_lettre(const char c)
{
	/*
	** trouver_lettre('a') => 0
	** trouver_lettre('A') => 0
	** trouver_lettre('b') => 1
	** trouver_lettre('Z') => 25
	** trouver_lettre('É') => 26
	**/

	unsigned char l = toupper(c) - 'A';

	return l > PATIENT_ARRAY_ENTRIES - 1 ? PATIENT_ARRAY_ENTRIES - 1 : l;
}

/* Trouver la lettre a afficher pour l'indice n de pa
 * (inverse de trouver_lettre) */
char trouver_lettre_affichage(const unsigned char n)
{
	/* n ∈ ℕ et 0 ≤ n ≤ 26 */
	/*
	** trouver_lettre_affichage(0)  => 'A'
	** trouver_lettre_affichage(2)  => 'C'
	** ...
	** trouver_lettre_affichage(25) => 'Z'
	** trouver_lettre_affichage(26) => '?'
	**/

	return n < 26 ? n + 'A' : '?';
}

/* Ouvrir le fichier en mode "mode" */
FILE *ouvrir_fichier(const char *filename, const char *mode)
{
	FILE *f = fopen(filename, mode);

	if (!f) {
		fprintf(stderr, "L'ouverture du fichier des patiens \"%s\" a "
			"echou" E_ACUTE ".\n", filename);
		fputs("Si ce n'est pas le nom de votre fichier,\n"
			"veuiller le passer comme argument " A_GRAVE " ce "
			"programme, par "
			"exemple:\n", stderr);
		fprintf(stderr, "%s mon_fichier.txt\n\n", get_program_name());
	}

	return f;
}

/* Affecter NULL à tous les éléments du tableau pa */
void init_patients_array(liste_d *pa[])
{
	unsigned char i;

	for (i = 0; i < PATIENT_ARRAY_ENTRIES; ++i)
		/* Initialization */
		pa[i] = NULL;

	return;
}

/* Allouer de la memoire puor 1 patient_s, et le retourner */
patient_s *patient_allocate(void)
{
	patient_s *p = malloc(sizeof(patient_s));
	if (!p) {
		fputs("Can't allocate memory!\n", stderr);
		exit(EXIT_FAILURE);
	}
	return p;
}

/* Allouer de la memoire puor 1 medicaments_s, et le retourner */
medicaments_s *meds_allocate(void)
{
	medicaments_s *m = malloc(sizeof(medicaments_s));
	if (!m) {
		fputs("Can't allocate memory!\n", stderr);
		exit(EXIT_FAILURE);
	}
	return m;
}

/* Crée et retourne un patient_s avec np et meds comme membres */
patient_s *patient_creer_element(char *np, sliste *meds)
{
	patient_s *q;

	/* Allouer une nouvel element et l'initialiser */
	q = patient_allocate();
	q->nomprenom = np;
	q->medicaments = meds;

	return q;
}

/* Crée et retourne un medicaments_s avec nom et num comme membres */
medicaments_s *meds_creer_element(char *nom, unsigned short num)
{
	medicaments_s *m;

	/* Allouer une nouvel element et l'initialiser */
	if (num > 0) {
		m = meds_allocate();
		m->medicam = nom;
		m->quant = num;
	} else {
		fputs("meds_creer_element: vous avez essay" E_ACUTE " "
			"d'ajouter un medicament avec quantit" E_ACUTE " "
			"<= 0.\n", stderr);
		m = NULL;
	}

	return m;
}
