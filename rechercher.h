#ifndef PROJET_PATIENTS_RECHERCHER_H
# define PROJET_PATIENTS_RECHERCHER_H

# include "struct.h"

/**
 * Cette fonction retourne une liste bidirectionnelle (dliste) contenant tous
 * les résultats. Elle ne fait pas l'affichage.
 */
liste_d *recherher_v2(liste_d *pa[], char *str);

/**
 * Cette fonction est similaire à recherher_v2, mais elle retourne une liste
 * bidirectionnelle (liste_d) contenant pas les resultats, mais les elements
 * de la liste des patients d'une lettre qui contiennent (les element) les
 * résultats, c'est à dire les (dliste *), et pas leurs data.
 * Cela est utile si on veut supprimer ces résultats de pa directement.
 */
liste_d *recherher_v2_listelements(liste_d *pa[], char *str);

/**
 * Cette fonction fait la conversion du resultat de recherher_v2_listelements
 * vers le resultat de recherher_v2.
 * Cela est utile si on veut par exemple simplement afficher ces resultats.
 * Notez que cette conversion est possible seulement dans ce sens,
 * i.e., la fonction n'es pas bijective.
 */
liste_d *recherher_v2_listelements_to_elements(liste_d *r_le);

/**
 * Cette fonction retourne TRUE si la chaine de caractères src est égale à
 * rchrch. On n'a pas utilisé strcmp() car on veut retourner 1 si s1 et s2 ne
 * diffèrent que par la capitalization.
 * On veut aussi retourner 1 si la longueur de s1 > longueur de s2,
 * e.g., match("Foo Bar", "foo") = 1
 * Cette fonction n'est clairement pas commutative.
 */
unsigned char match(const char *src, const char *rchrch);

#endif /* PROJET_PATIENTS_RECHERCHER_H */
