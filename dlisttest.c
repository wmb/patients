#include "dliste.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

void print_list(liste_d *l);

liste_d *dliste_ajouter_trie(liste_d *list, void *data);

const int a[] = {7, 11, 12, 3, 6, 15, 5, 10, 1, 16, 4, 8, 9, 14, 2, 13};

int main(void)
{
	size_t i;
	dliste *fifth;
	liste_d *copy = NULL;
	liste_d *l = NULL;
	void *tmp[2];
	liste_d *one_element_list;

	for (i = 0; i < sizeof a / sizeof a[0]; ++i)
		l = dliste_ajouter_trie(l, (void *) (long) a[i]);
	puts("Printing the list:");
	print_list(l);

	puts("Add 42 after the fifth element:");
	for (fifth = l->tete, i = 0; i < 5 - 1 && fifth; ++i)
		fifth = fifth->next;
	l = dliste_ajouter_apres(l, fifth, (void *) (long) 42);
	print_list(l);

	puts("Remove what you added:");
	dliste_supprimer(l, fifth->next);
	print_list(l);

	puts("Add 99 before the 5th element:");
	for (fifth = l->tete, i = 0; i < 5 - 1 && fifth; ++i)
		fifth = fifth->next;
	l = dliste_ajouter_avant(l, fifth, (void *) (long) 99);
	print_list(l);

	puts("Remove what you added:");
	dliste_supprimer(l, fifth->prev);
	print_list(l);

	puts("Remove first element:");
	tmp[0] = dliste_supprimer(l, l->tete);
	print_list(l);

	puts("Remove last element:");
	tmp[1] = dliste_supprimer(l, l->queue);
	print_list(l);

	puts("Add the elements you removed:");
	l = dliste_ajouter_tete(l, tmp[0]);
	l = dliste_ajouter(l, tmp[1]);
	print_list(l);

	while (l->tete) {
		copy = dliste_ajouter(copy, dliste_supprimer(l, l->tete));
		/* We can also do:
		** copy = dliste_ajouter_tete(copy,
		**	dliste_supprimer(l, l->queue));
		** which is equivalent.
		** And we can swap one of them to reverse the order. */
	}

	puts("L: (should be empty now)");
	print_list(l);
	puts("Copy: (should be exactly what L was before)");
	print_list(copy);


	/* Emptying the lists */
	copy = dliste_liberer_liste(copy);
	l = dliste_liberer_liste(l);

	one_element_list = dliste_ajouter(NULL, (void *) (long) 1337);
	puts("One-element list:");
	print_list(one_element_list);
	puts("Removing that one element:");
	dliste_supprimer(one_element_list, one_element_list->queue);
	print_list(one_element_list);

	puts("avant(42) on empty list:");
	one_element_list = dliste_ajouter_avant(one_element_list, one_element_list->tete, (void *) (long) 42);
	print_list(one_element_list);
	puts("avant(44) on one-element-list:");
	one_element_list = dliste_ajouter_avant(one_element_list, one_element_list->tete, (void *) (long) 44);
	print_list(one_element_list);

	while (one_element_list->nbr_elements > 0)
		dliste_supprimer(one_element_list, one_element_list->tete);
	puts("Should be empty now:");
	print_list(one_element_list);

	puts("apres(42) on empty list:");
	one_element_list = dliste_ajouter_apres(one_element_list, one_element_list->tete, (void *) (long) 42);
	print_list(one_element_list);
	puts("apres(44) on one-element-list:");
	one_element_list = dliste_ajouter_apres(one_element_list, one_element_list->tete, (void *) (long) 44);
	print_list(one_element_list);

	one_element_list = dliste_liberer_liste(one_element_list);

	return 0;
}

liste_d *dliste_ajouter_trie(liste_d *list, void *data)
{
	int a;
	bool found = false;
	dliste *l;

	l = list ? list->tete : NULL;
	a = (int) (long) (void *) data;

	while (l && !found) {
		int b = (int) (long) (void *) l->data;
		if (a < b)
			found = true;
		else
			l = l->next;
	}
	if (found)
		list = dliste_ajouter_avant(list, l, data);
	else
		list = dliste_ajouter(list, data);

	return list;
}

void print_list(liste_d *l)
{
	dliste *t, *q;
	signed char i = 8;

	if (!l) {
		puts("Erreur: l == NULL.");
		return;
	}

	t = l->tete;
	q = l->queue;

	printf("Elements: %hu\n", l->nbr_elements);

	if (!(t && q)) {
		puts("Empty list");
		return;
	}

	fputs("-->  ", stdout);
	while (t) {
		printf("%4d ", (int) (long) t->data);
		t = t->next;
		i += 5;
	}

	fputs(" -->\n<--  ", stdout);
	while (q) {
		printf("%4d ", (int) (long) q->data);
		q = q->prev;
	}
	puts(" <--");

	while (i-- >= 0)
		putchar('-');
	putchar('\n');

	return;
}
