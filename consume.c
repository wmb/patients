#include "consume.h"
#include "general_funcs.h"
#include "print_v3.h"
#include "rechercher.h"
#include "struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void consommer(liste_d *resultats)
{
	char *med_a_consommer;
	unsigned short med_quant;
	patient_s *p;

	if (!resultats || resultats->nbr_elements == 0)
		return;

	if (resultats->nbr_elements > 1) {
		/* Il y'a plus qu'un seul patient. */
		fprintf(stderr, "consommer: Votre recherche correspond "
			A_GRAVE " %hu r" E_ACUTE "sultats. S'il vous pla"
			I_CIRC "t pr" E_ACUTE "cisez votre recherche.\n",
			resultats->nbr_elements);
		puts("Resultats de la recherche :");
		afficher_liste_patients(resultats, 0, FALSE, FALSE);
		return;
	}

	p = resultats->tete->data;

	/* Afficher le patient avant la consommation */
	fputs("Avant consommation :\n", stderr);
	afficher_un_patient(p, -1, TRUE);

	/* Allouer de l'espace memoir pour le nom du médicament (32 octets) */
	med_a_consommer = calloc(32, 1);
	if (!med_a_consommer) {
		fputs("Memoire insuffisante!\n", stderr);
		return;
	}

	/* Demander de l'utilisateur d'entre le nom du médicament */
	fputs("Donnez le nom du medicament " A_GRAVE " consommer : ", stdout);
	scanf("%31s", med_a_consommer);

	/* Demander de l'utilisateur d'entre la quantité a consommer */
	fputs("Donnez la quantit" E_ACUTE " " A_GRAVE " consommer : ",
		stdout);
	scanf("%hu", &med_quant);

	/* Appeler la fonction qui fait la consommation */
	consommer_v2(p, med_a_consommer, med_quant);

	/* Liberer l'espace alloué */
	free(med_a_consommer);

	/* Afficher le patient après la consommation */
	fputs("Apr" E_GRAVE "s consommation :\n", stderr);
	afficher_un_patient(p, -1, TRUE);

	return;
}

void consommer_v2(patient_s *p, char *med, unsigned short quant)
{
	unsigned short old_quant;
	medicaments_s *m;
	sliste *m_list;
	unsigned char trouve = FALSE;

	if (!med)
		return;

	m_list = p->medicaments;

	while (m_list && trouve == FALSE) {

		m = (medicaments_s *) m_list->data;

		if (match(m->medicam, med))
			trouve = TRUE;
		else
			m_list = m_list->next;
	}

	if (!trouve) {
		fprintf(stderr, "Medicament \"%s\" non trouv" E_ACUTE".\n",
			med);
		return;
	}

	old_quant = m->quant;

	if (old_quant > quant) {
		m->quant -= quant;
	} else {
		if (old_quant < quant)
			/* L'utilisateur à essayé de consommer plus de
			** médicaments qu'il avait. Afficher un message
			** d'erreur. */
			fputs("Vous avez essay" E_ACUTE " de consommer plus "
				"de m" E_ACUTE "dicaments que vous aviez.\n",
				stderr);

		/* Supprimer m_list de la list (p->medicaments) */
		free(m->medicam);
		free(m);
		p->medicaments = sliste_supprimer(p->medicaments, m_list);
	}

	return;
}
