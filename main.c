#include "consume.h"
#include "general_funcs.h"
#include "liberer.h"
#include "lire_fichier.h"
#include "print_v3.h"
#include "rechercher.h"
#include "struct.h"
#include "supprimer.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AUTHOR_NAME "Walid M. Boudelal"
#define MATRICULE "201500010459"

/* Le fichier à utiliser par defaut */
#define DEFAULT_FILENAME "testfile.txt"

unsigned char read_response(char **args);
void auteur(void);
void repl(liste_d *pa[]);
void start(const char *nom_fichier);
void afficher_aide(const char *commande);

int main(int argc, char *argv[])
{
	const char *nom_fichier = argc > 1 ? argv[1] : DEFAULT_FILENAME;
	set_program_name(argv[0]);

	start(nom_fichier);

	exit(EXIT_SUCCESS);
}

void start(const char *nom_fichier)
{
	FILE *f;
	liste_d *patients_array[PATIENT_ARRAY_ENTRIES];

	WINDOWS_FIX_CHARSET();

	auteur();

	f = ouvrir_fichier(nom_fichier, "r");
	if (!f)
		goto fichier_non_trouve;

	init_patients_array(patients_array);

	lire_fichier_v2(f, patients_array);

	fclose(f);

	repl(patients_array);

	liberer_tout(patients_array);

fichier_non_trouve:
	WINDOWS_PREVENT_CLOSING_CMD();

	return;
}

void repl(liste_d *pa[])
{
	unsigned char quit = FALSE;
	char *args;
	const char *prompt = "(patients) ";

	puts("Entrez ? pour afficher l'aide.");
	while (quit == FALSE) {
		liste_d *resultats;
		patient_s *p;
		short tmp;
		unsigned char lettre;
		unsigned char resp;

		fputs(prompt, stdout);
		resp = read_response(&args);

		switch (resp) {
		case 1:
			tmp = (short) atoi(args);
			afficher_patients((const liste_d **) pa, tmp);
			break;
		case 2:
			if (*args == '\0') {
				afficher_aide("2");
				break;
			}

			/* TODO: Rechercher non seulement le nom, mais aussi le
			** prénom. */
			resultats = recherher_v2(pa, args);
			if (!resultats) {
				fprintf(stderr, "Aucun patient trouv" E_ACUTE
					" pour la recherche \"%s\"\n", args);
			}
			afficher_liste_patients(resultats, 0, TRUE, TRUE);
			resultats = dliste_liberer_liste(resultats);
			break;
		case 3:
			if (*args == '\0') {
				afficher_aide("3");
				break;
			}
			if (!(p = lire_ligne(args)))
				break;
			lettre = trouver_lettre(*(p->nomprenom));
			pa[lettre] = ajouter_patient_trie(pa[lettre], p);
			break;
		case 4:
			if (*args == '\0') {
				afficher_aide("4");
				break;
			}
			supprimer(pa, args);
			break;
		case 5:
			if (*args == '\0') {
				afficher_aide("2");
				break;
			}

			resultats = recherher_v2(pa, args);
			if (!resultats) {
				fprintf(stderr, "Aucun patient trouv" E_ACUTE
					" pour la recherche \"%s\"\n", args);
				break;
			}
			consommer(resultats);
			resultats = dliste_liberer_liste(resultats);
			break;
		case 6:
			quit = TRUE;
			break;
		case 7:
			afficher_aide(args);
			break;
		case 16:
			/* Sortir du switch
			** et demander une nouvelle commande */
			break;
		default:
			fputs("Operation inconnue. Entrez ? pour afficher "
				"l'aide.\n", stderr);
			break;
		}
		free(args);
	}

	return;
}

unsigned char read_response(char **args)
{
	unsigned char r;
	int c;
	char buf[0xff];
	size_t i;
	size_t size;

	/* TODO: Just use fgets instead of this shit. */

	while (isspace(c = getchar()) && c != '\n' && c != EOF)
		continue;

	if (isdigit(c)) {
		r = c - '0';
	} else if (c == '?') {
		r = 7;
	} else if (tolower(c) == 'q') {
		r = 6;
	} else if (c == EOF || c == '\n') {
		/* No answer, try again */
		r = 16;
		*args = NULL;
		goto quit;
	} else {
		/* Unknown answer */
		r = 0;
	}

	i = 0;
	/* Skip whitespace */
	/* Actually, don't skip any whitespace. Let the receiving function deal with it itself. */
	/*
	while (isspace(c = getchar()) && c != '\n')
		continue;
	buf[i++] = c;
	*/
	while ((c = getchar()) != EOF && c != '\n' && i < 0xfe) {
		buf[i++] = c;
	}
	size = i;
	*args = malloc(size + 1);
	if (!(*args)) {
		fputs("Memoire unsuffisante\n", stderr);
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < size; ++i) {
		(*args)[i] = buf[i];
	}
	(*args)[i] = '\0';

quit:
	return r;
}

/**
 * Cette fonction affiche l'aide sur les commandes.
 */
void afficher_aide(const char *commande)
{
	unsigned char n = 0;
	char c;
	const char *help[] = {
NULL,
"\t1 [nombre de caract" E_GRAVE "res]\n"
	"\t\tAfficher la liste des patients :\n"
	"\t\t\tSi [nombre de caract" E_GRAVE "res] est donn" E_ACUTE ",\n"
	"\t\t\t\talors demander une confirmation chaque [nombre de caract"
	E_GRAVE "res].\n"
	"\t\t\tSi [nombre de caract" E_GRAVE "res] est < 0,\n"
	"\t\t\t\talors afficher tous les patients sans confirmation.\n"
	"\t\t\tSi [nombre de caract" E_GRAVE "res] est = 0, ou n'est pas donn"
	E_ACUTE ",\n"
	"\t\t\t\talors utiliser le [nombre de caract" E_GRAVE "res] par d"
	E_ACUTE "faut = 4.\n\n",
"\t2 <requ" E_CIRC "te de recherche>\n"
	"\t\tRechercher un patient dont le nom commence par <requ" E_CIRC
	"te de recherche>.\n"
	"\t\tLa recherche ne prend pas en compte la capitalisation : \n"
	"\t\t\t\"2 RECH\", \"2 ReCh\", et \"2 rech\" sont equivalents.\n\n",
"\t3 <nom> <liste meds>\n"
	"\t\tAjouter un patient " A_GRAVE " la liste. La syntax est la m"
	E_CIRC "me que celle dans le fichier.\n\n",
"\t4[!] <nom du patient>\n"
	"\t\tSupprimer un patient de la liste.\n"
	"\t\tUtilisez \"4!\" pour supprimer plusieurs patients " A_GRAVE " la"
	" fois.\n\n",
"\t5 <nom du patient>\n"
	"\t\tConsommer un m" E_ACUTE "dicament donn" E_ACUTE ".\n\n",
"\t6 ou q\n"
	"\t\tQuitter le programme.\n\n",
"\t? [commande]\n"
	"\t\tAfficher l'aide " A_GRAVE " propos de la commande "
	"[commande] si [commande] est donn" E_GRAVE "e,\n"
	"\t\tsinon toutes les commandes sont affich" E_ACUTE "s.\n\n",
"NOTE :\n"
	"\tLes espaces entre la commande et ses arguments ne sont pas "
	"importants,\n"
	"\tc'est " A_GRAVE " dire que les commandes suivantes :\n"
	"\t\t\"2 Pat\"\n"
	"\t\t\"2  Pat\"\n"
	"\t\t\"2    Pat\"\n"
	"\t\t\"2Pat\"\n"
	"\tsont toutes equivalentes (rechercher les patients dont le nom "
	"commence par \"Pat\").\n\n"};
	const char *examples[] = {
NULL,
"\t1\n"
"\t1 0\n"
"\t1 4\n"
	"\t\tAfficher la liste des patients, en s'arretant chaque 4 caract"
	E_GRAVE "res affich" E_ACUTE "s (A..D, E..H, I..L, etc.).\n"
"\t1 10\n"
	"\t\tAfficher la liste des patients, en s'arretant chaque 10 caract"
	E_GRAVE "res affich" E_ACUTE "s (A..J, K..T).\n"
"\t1 -1\n"
	"\t\tAfficher la liste des patients jusqu'" A_GRAVE " la fin, sans "
	"arr" E_CIRC "t.\n",
"\t2 Nom\n"
"\t2 nom\n"
"\t2 NOM\n"
	"\t\tRechercher les patients dont le nom commence par \"nom\", et les"
	" afficher (avec leur m" E_ACUTE "dicaments.\n",
"\t3 John Doe M1 5 M2 7 M3 11 M4 13 M_Vide\n"
	"\t\tAjouter le patient \"John Doe\", qui poss" E_GRAVE "de les m"
	E_ACUTE "dicaments M1, M2, M3, et M4,\n"
	"\t\tavec les quantit" E_ACUTE "s 2, 3, 5, 7, 11, et 13, "
	"respectivement.\n"
	"\t\t(on aura un avertissement pour M_Vide).\n"
"\t3 Patient SansMedicaments\n"
	"\t\tAjouter le patient \"Patient SansMedicaments\", qui ne poss"
	E_GRAVE "de aucun m" E_ACUTE "dicament.\n",
"\t4 john doe\n"
	"\t\tRechercher dans la liste des patient un patient nomm" E_ACUTE
	" \"john doe\", et le supprimer de la liste s'il existe.\n"
"\t4! jo\n"
	"\t\tSupprimer tous les patients dont le nom commence par \"jo\" de "
	"liste.\n",
"\t5 john doe\n"
	"\t\tConsommer des m" E_GRAVE "dicaments pour le patient "
	"\"john doe\".\n",
"\t6\n"
"\tq\n"
"\tQ\n"
	"\t\tQuitter ce programme.\n",
"\t?\n"
	"\t\tAfficher un message d'aide g" E_ACUTE "n" E_ACUTE "ral pour "
	"toutes les commandes.\n"
"\t? ?\n"
	"\t\tAfficher l'aide et des exemples d'utilisation pour la commande "
	"\"?\".\n"};

	if (!commande)
		goto print_help;

	while ((c = *commande++) != '\0')
		if (isdigit(c)) {
			n = c - '0';
			break;
		} else if (c == 'q') {
			n = 6;
			break;
		} else if (c == '?') {
			n = 7;
			break;
		}

print_help:
	if (0 < n && n < sizeof help / sizeof help[0] - 1) {
		puts("Utilisation :");
		fputs(help[n], stdout);
		puts("Exemples :");
		fputs(examples[n], stdout);
	} else {
		unsigned char i;
		puts("\nVeuiller entrer l'un des commandes suivantes:\n");
		for (i = 1; i < sizeof help / sizeof help[0]; ++i)
			fputs(help[i], stdout);
	}

	return;
}

void auteur(void)
{
	fputs("Copyright (C) 2017 " AUTHOR_NAME "\n"
		MATRICULE "\n"
		"Section B, Groupe 2\n"
		"L2 ACAD\n"
		"USTHB\n\n",
		stderr);
	return;
}
