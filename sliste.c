#include "sliste.h"
#include <stdio.h>
#include <stdlib.h>


/* Allouer de l'espace pour une sliste */
static sliste *sliste_allouer(void);

/* Ajouter un element à la fin de la liste */
sliste *sliste_ajouter(sliste *list, void *data)
{
	sliste *new_list;

	new_list = sliste_allouer();
	new_list->data = data;
	new_list->next = NULL;

	if (list) {
		sliste *queue = list;
		while (queue->next) {
			queue = queue->next;
		}
		queue->next = new_list;
	} else {
		list = new_list;
	}

	return list;
}

/* Supprimmer l'element elem de la liste list */
sliste *sliste_supprimer(sliste *list, sliste *elem)
{
	sliste *prev = list;

	if (!(list && elem))
		return list;

	if (elem == list) {
		list = elem->next;
	} else {
		while (prev->next != elem && prev->next != NULL) {
			prev = prev->next;
		}
		if (!prev->next) {
			return list;
		}
		prev->next = elem->next;
	}

	elem->next = NULL;
	free(elem);

	return list;
}

static sliste *sliste_allouer(void)
{
	sliste *const a = malloc(sizeof(sliste));

	if (!a) {
		fputs("sliste_allouer: Memoire unsiffisante!\n", stderr);
		exit(EXIT_FAILURE);
	}

	return a;
}
