#ifndef PATIENT_GENERAL_FUNCS_H
# define PATIENT_GENERAL_FUNCS_H

# include "struct.h"
# include <ctype.h>
# include <stdio.h>

# if defined(__WIN32)
#  define WINDOWS_PREVENT_CLOSING_CMD() \
	do {\
		putchar('\n'); \
		do \
			fputs("Entrer Q pour quitter. ", stderr); \
		while (toupper(getchar()) != 'Q'); \
	} while (0)
# else /* !defined(__WIN32) */
#  define WINDOWS_PREVENT_CLOSING_CMD()
# endif /* defined(__WIN32) */


/* Le nom de l'executable de ce programme, pour l'afficher dans les messages
 * d'erreurs. */
void set_program_name(char *pname);
char *get_program_name(void);

/* Trouver la quel element de pa va avec la lettre c */
unsigned char trouver_lettre(const char c);

/* Trouver la lettre a afficher pour l'indice n de pa
 * (inverse de trouver_lettre) */
char trouver_lettre_affichage(const unsigned char n);

/* Ouvrir le fichier en mode "mode" */
FILE *ouvrir_fichier(const char *filename, const char *mode);

/* Mettre NULL sur tous les elements de pa */
void init_patients_array(liste_d *pa[]);

/* Allouer de la memoire puor 1 patient_s, et le retourner */
patient_s *patient_allocate(void);

/* Allouer de la memoire puor 1 medicaments_s, et le retourner */
medicaments_s *meds_allocate(void);

/* Crée et retourne un patient_s avec np et meds comme membres */
patient_s *patient_creer_element(char *np, sliste *meds);

/* Crée et retourne un medicaments_s avec nom et num comme membres */
medicaments_s *meds_creer_element(char *nom, unsigned short num);

/* Retourne le patients avec le nom nom */
patient_s *get_patient_by_name(dliste *list, const char *nom);


#endif /* PATIENT_GENERAL_FUNCS_H */
