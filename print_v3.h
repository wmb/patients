#ifndef PROJET_PATIENTS_AFFICHER_V2
# define PROJET_PATIENTS_AFFICHER_V2

#include "struct.h"


/* Prototypes des fonctions */

/**
 * Affiche la liste des patients, n patients à la fois (4 par défaut).
 */
void afficher_patients(const liste_d *liste_patients[], short n);

/**
 * Affiche une dliste des patients, sans afficher leurs médicaments.
 */
void afficher_liste_patients(const liste_d *liste_patients, unsigned n,
	unsigned char afficher_meds, unsigned char afficher_numeros);

/**
 * Afiche une seule entrée dans la liste des patients (un patient).
 */
void afficher_un_patient(const patient_s *patient, int n,
	unsigned char afficher_meds);
/**
 * Affiche la liste des medicaments pour un patient.
 */
void afficher_medicaments(const sliste *meds);

/**
 * Affiche un seul medicament.
 */
void afficher_un_medicament(const medicaments_s *med);

/**
 * Lis la réponse de l'utilisateur (continuer a afficher ou pas).
 */
unsigned char lire_reponse(const char *question);


#endif /* PROJET_PATIENTS_AFFICHER_V2 */
