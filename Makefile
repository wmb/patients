CC = gcc
CFLAGS = -Wall -Wextra -Werror -pedantic-errors -std=c99
CFLAGS += -Ofast -march=native
#CFLAGS += -g -O0 -fstack-protector-all -fstack-check

PROGNAME = prj
OBJS = consume.o \
	dliste.o \
	general_funcs.o \
	liberer.o \
	lire_fichier.o \
	main.o \
	print_v3.o \
	rechercher.o \
	sliste.o \
	supprimer.o

.PHONY: all listtest clean clean-all

all: $(PROGNAME)


$(PROGNAME): $(OBJS)
	$(CC) -o '$@' $^ $(LIBS) $(LDFLAGS) $(LDLIBS)


consume.o: consume.c consume.h general_funcs.h print_v3.h rechercher.h struct.h

dliste.o: dliste.c dliste.h

general_funcs.o: general_funcs.c general_funcs.h struct.h

liberer.o: liberer.c liberer.h struct.h

lire_fichier.o: lire_fichier.c lire_fichier.h struct.h

main.o: main.c \
	consume.h \
	general_funcs.h \
	liberer.h \
	lire_fichier.h \
	print_v3.h \
	rechercher.h \
	struct.h \
	supprimer.h

print_v3.o: print_v3.c print_v3.h general_funcs.h struct.h

rechercher.o: rechercher.c general_funcs.h struct.h

sliste.o: sliste.c sliste.h

supprimer.o: supprimer.c supprimer.h general_funcs.h liberer.h print_v3.h rechercher.h struct.h

struct.h: unicode.h


listtest: slisttest dlisttest

dlisttest: dlisttest.o dliste.o
dlisttest.o: dlisttest.c dliste.c dliste.h

slisttest: slisttest.o sliste.o
slisttest.o: slisttest.c sliste.c sliste.h


clean:
	-@rm -f ./*.o ./*~ ./.*~
clean-all: clean
	-@rm -f ./$(PROGNAME) dlisttest slisttest
