#ifndef PROJET_PATIENTS_LIBERER_H
# define PROJET_PATIENTS_LIBERER_H

#include "struct.h"

void liberer_tout(liste_d *liste_patients[]);

void liberer_lettre(liste_d *lettre);

void liberer_patient(patient_s *patient);

void liberer_liste_meds(sliste *meds);

void liberer_medicament(medicaments_s *med);

#endif /* PROJET_PATIENTS_LIBERER_H */
