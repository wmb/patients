#include "general_funcs.h"
#include "rechercher.h"
#include <ctype.h>
#include <string.h>

/**
 * Cette fonction retourne une liste bidirectionnelle (liste_d) contenant tous
 * les résultats. Elle ne fait pas l'affichage.
 */
liste_d *recherher_v2(liste_d *pa[], char *str)
{
	liste_d *r = NULL;
	dliste *patients;
	unsigned char lettre;
	int i;

	if (!str)
		return NULL;

	/* Supprimer les blancs au début de str (s'ils existent) */
	while (isspace(*str))
		++str;

	/* Supprimer les blancs à la fin de str (s'ils existent) */
	for (i = strlen(str) - 1; i >= 0 && isspace(str[i]); --i)
		/* On fait la suppression simplement en ajoutant des '\0' */
		str[i] = '\0';

	lettre = trouver_lettre(str[0]);
	if (!pa[lettre])
		return NULL;
	patients = (pa[lettre])->tete;

	while (patients) {
		patient_s *p = patients->data;
		if (match(p->nomprenom, str)) {
			/* Ajouter p aux resultats */
			r = dliste_ajouter(r, p);
		}
		/* Avancer */
		patients = patients->next;
	}

	return r;
}


/**
 * Cette fonction est similaire à recherher_v2, mais elle retourne une liste
 * bidirectionnelle (liste_d) contenant pas les resultats, mais les elements
 * de la liste des patients d'une lettre qui contiennent (les element) les
 * résultats, c'est à dire les (dliste *), et pas leurs data.
 * Cela est utile si on veut supprimer ces résultats de pa directement.
 */
liste_d *recherher_v2_listelements(liste_d *pa[], char *str)
{
	liste_d *r = NULL;
	dliste *patients;
	unsigned char lettre;
	int i;

	if (!str)
		return NULL;

	/* Supprimer les blancs au début de str (s'ils existent) */
	while (isspace(*str))
		++str;

	/* Supprimer les blancs à la fin de str (s'ils existent) */
	for (i = strlen(str) - 1; i >= 0 && isspace(str[i]); --i)
		/* On fait la suppression simplement en ajoutant des '\0' */
		str[i] = '\0';

	lettre = trouver_lettre(str[0]);
	if (!pa[lettre])
		return NULL;
	patients = (pa[lettre])->tete;

	while (patients) {
		patient_s *p = patients->data;
		if (match(p->nomprenom, str)) {
			/* Ajouter l'element de la liste contenant p
			** aux resultats */
			r = dliste_ajouter(r, patients);
		}
		/* Avancer */
		patients = patients->next;
	}

	return r;
}

/**
 * Cette fonction fait la conversion du resultat de recherher_v2_listelements
 * vers le resultat de recherher_v2.
 * Cela est utile si on veut par exemple simplement afficher ces resultats.
 * Notez que cette conversion est possible seulement dans ce sens,
 * i.e., la fonction n'es pas bijective.
 * Notez aussi qu'apres l'utilisation de cette fonction, vous perderez
 * l'ancien resultat, et il sera automatiquement libéré.
 */
liste_d *recherher_v2_listelements_to_elements(liste_d *r_le)
{
	liste_d *r_e = NULL;

	if (!r_le)
		return NULL;

	while (r_le->nbr_elements > 0) {
		dliste *l_p = dliste_supprimer(r_le, r_le->queue);
		if (!l_p)
			continue;
		r_e = dliste_ajouter_tete(r_e, l_p->data);
	}

	dliste_liberer_liste(r_le);

	return r_e;
}

/**
 * Cette fonction retourne TRUE si la chaine de caractères src est égale à
 * rchrch. On n'a pas utilisé strcmp() car on veut retourner 1 si s1 et s2 ne
 * diffèrent que par la capitalization.
 * On veut aussi retourner 1 si la longueur de s1 > longueur de s2,
 * e.g., match("Foo Bar", "foo") = 1
 * Cette fonction n'est clairement pas commutative.
 */
unsigned char match(const char *src, const char *rchrch)
{
	unsigned char m = TRUE;
	size_t i;

	if (!(src && rchrch))
		m = FALSE;

	/* Une chaîne vide ne correspond pas */
	if (m == TRUE && !*rchrch)
		m = FALSE;

	for (i = 0; m == TRUE && src[i] && rchrch[i]; ++i) {
		if (tolower(src[i]) != tolower(rchrch[i]))
			m = FALSE;
	}

	/* Si on a atteint la fin de src, mais pas de rchrch, alors on retourne
	** faux */
	/*
	if (!src[i] && rchrch[i])
		m = FALSE;
	*/
	/* Le code commenté ci-dessus est équivalent à la ligne ci-dessous: */
	m = m && (src[i] || !rchrch[i]);

	return m;
}
