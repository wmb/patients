#include "dliste.h"
#include <stdio.h>
#include <stdlib.h>


/* Allouer de l'espace pour une dliste */
static dliste *dliste_allouer_element(void);
static liste_d *dliste_allouer_liste(void);


liste_d *dliste_creer_liste(void *data)
{
	liste_d *l = dliste_allouer_liste();
	dliste *e = dliste_allouer_element();

	e->data = data;
	e->next = e->prev = NULL;
	l->tete = l->queue = e;
	l->nbr_elements = 1u;

	return l;
}

/* Ajouter un element à la fin de la liste */
liste_d *dliste_ajouter(liste_d *list, void *data)
{
	dliste *new_list;

	if (!(list && list->tete))
		/* TODO: Create the list instead of failing */
		return dliste_creer_liste(data);

	new_list = dliste_allouer_element();
	new_list->data = data;
	new_list->next = NULL;
	new_list->prev = list->queue;

	if (list->queue)
		list->queue->next = new_list;
	else
		list->tete = new_list;

	list->queue = new_list;
	++list->nbr_elements;

	return list;
}

/* Ajouter un element x à la tête de la liste liste */
liste_d *dliste_ajouter_tete(liste_d *list, void *x)
{
	dliste *new_list;

	if (!list)
		/* TODO: Don't just exit. Create it if you have to. */
		return dliste_creer_liste(x);

	new_list = dliste_allouer_element();
	new_list->data = x;
	new_list->prev = NULL;
	new_list->next = list->tete;
	if (list->tete)
		list->tete->prev = new_list;
	else
		list->queue = new_list;

	list->tete = new_list;
	++list->nbr_elements;

	return list;
}

liste_d *dliste_ajouter_apres(liste_d *list, dliste *elem, void *x)
{
	dliste *new_list;
	dliste *elemnext;

	if (!list)
		return dliste_creer_liste(x);

	if (list->nbr_elements == 0)
		return dliste_ajouter_tete(list, x);

	if (!elem) {
		/* Si elem == NULL alors ajouter à la tête de la liste */
		fputs("dliste_ajouter_apres: calling dliste_ajouter()\n",
			stderr);
		return dliste_ajouter_tete(list, x);
	}

	new_list = dliste_allouer_element();
	new_list->data = x;

	elemnext = elem->next;
	new_list->next = elemnext;
	if (elemnext)
		elemnext->prev = new_list;
	else
		list->queue = new_list;

	elem->next = new_list;
	new_list->prev = elem;

	++list->nbr_elements;

	return list;
}

liste_d *dliste_ajouter_avant(liste_d *list, dliste *elem, void *x)
{
	dliste *new_list;
	dliste *elemprev;

	if (!list)
		return dliste_creer_liste(x);

	if (list->nbr_elements == 0)
		return dliste_ajouter_tete(list, x);

	if (!elem) {
		/* Si elem == NULL alors ajouter à la fin de la liste */
		fputs("dliste_ajouter_avant: calling dliste_ajouter_tete()\n",
			stderr);
		return dliste_ajouter(list, x);
	}

	new_list = dliste_allouer_element();
	new_list->data = x;
	elemprev = elem->prev;

	new_list->prev = elemprev;
	if (elemprev)
		elemprev->next = new_list;
	else
		list->tete = new_list;

	new_list->next = elem;
	elem->prev = new_list;

	++list->nbr_elements;

	return list;
}

/* Supprimmer l'element x de la liste list, et retourner un pointeur vers ce
 * qu'il avait dans le champ data. */
void *dliste_supprimer(liste_d *list, dliste *x)
{
	dliste *xnext = NULL;
	dliste *xprev = NULL;
	void *data;

	if (!(list && list->tete && x))
		return x ? x->data : NULL;

	data = x->data;
	xnext = x->next;
	xprev = x->prev;

	if (xprev)
		xprev->next = xnext;
	else
		/* x was the head */
		list->tete = xnext;

	if (xnext)
		xnext->prev = xprev;
	else
		/* x was the tail */
		list->queue = xprev;

	free(x);

	--list->nbr_elements;

	return data;
}

/* Liberer la liste seulement (sans toucher les data) */
liste_d *dliste_liberer_liste(liste_d *list)
{
	if (!list)
		return list;

	while (list->nbr_elements > 0)
		dliste_supprimer(list, list->tete);

	free(list);

	return NULL;
}

static dliste *dliste_allouer_element(void)
{
	dliste *const a = malloc(sizeof(dliste));

	if (!a) {
		fputs("dliste_allouer_element: Memoire unsiffisante!\n",
			stderr);
		exit(EXIT_FAILURE);
	}

	return a;
}

static liste_d *dliste_allouer_liste(void)
{
	liste_d *const a = malloc(sizeof(liste_d));

	if (!a) {
		fputs("dliste_allouer_liste: Memoire unsiffisante!\n",
			stderr);
		exit(EXIT_FAILURE);
	}

	return a;
}
