#include "general_funcs.h"
#include "liberer.h"
#include "print_v3.h"
#include "rechercher.h"
#include "struct.h"
#include "supprimer.h"
#include <ctype.h>
#include <stdio.h>

void supprimer(liste_d *pa[], char *args)
{
	unsigned char supprimer_plusieurs = FALSE;
	liste_d *resultats;

	if (!args)
		return;

	if (*args == '!') {
		supprimer_plusieurs = TRUE;
		++args;
	}

	/* Sauter les blancs */
	while (*args && isspace(*args))
		++args;

	resultats = recherher_v2_listelements(pa, args);
	if (!resultats || resultats->nbr_elements == 0) {
		fprintf(stderr, "Aucun patient trouv" E_ACUTE " pour la "
			"recherche \"%s\"\n", args);
		goto quitter;
	}

	if (resultats->nbr_elements == 1 || supprimer_plusieurs) {
		supprimer_patients(pa, resultats);
		goto quitter;
	}

	fprintf(stderr, "consommer: Votre recherche correspond " A_GRAVE " "
			"%hu r" E_ACUTE "sultats. S'il vous pla" I_CIRC "t pr"
		E_ACUTE "cisez votre recherche.\n",
		resultats->nbr_elements);
	puts("Resultats de la recherche :");
	/* Préparer la liste pour être affichée */
	resultats = recherher_v2_listelements_to_elements(resultats);
	afficher_liste_patients(resultats, 0, FALSE, FALSE);

quitter:
	resultats = dliste_liberer_liste(resultats);
	return;
}

/**
 * Cette fonction est trop compliquée. Elle utilise une liste des pointeurs
 * vers des listes pour supprimer un element d'une autre liste.
 * Elle peut être ecrite d'une manière trés simple, mais ça marche ¯\_(ツ)_/¯.
 */
void supprimer_patients(liste_d *pa[], liste_d *patients)
{
	if (!(pa && patients))
		return;

	while (patients->nbr_elements > 0) {
		dliste *p_l = patients->queue->data;
		patient_s *p = p_l->data;
		unsigned char lettre = trouver_lettre(p->nomprenom[0]);

		dliste_supprimer(pa[lettre], p_l);
		liberer_patient(p);
		dliste_supprimer(patients, patients->queue);
	}

	return;
}
